package dev.val.dw_echo_server;

import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class EchoApplicationTest {
    private final EchoApplication application = new EchoApplication();

    @Test
    public void echo_returns_expected_value() {
        String echoValue = application.getEchoValue();
        assertThat(echoValue, equalTo("Echo"));
    }
}
