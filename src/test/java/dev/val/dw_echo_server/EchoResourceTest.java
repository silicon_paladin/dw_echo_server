package dev.val.dw_echo_server;


import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class EchoResourceTest {
    private static final String ECHO = "Echo";
    private EchoApplication application = mock(EchoApplication.class);
    private final EchoResource resource = new EchoResource(application);

    @Test
    public final void resource_calls_application() {
        when(application.getEchoValue()).thenReturn(ECHO);

        String echoValue = resource.echo();

        verify(application).getEchoValue();
        assertThat(echoValue, equalTo(ECHO));
    }

}