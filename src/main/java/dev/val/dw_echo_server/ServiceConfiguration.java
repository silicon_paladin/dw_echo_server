package dev.val.dw_echo_server;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

public class ServiceConfiguration extends Configuration {

    @JsonProperty("moo")
    private final String moo;

    public ServiceConfiguration(@JsonProperty("moo") String moo) {
        this.moo = moo;
    }
}
