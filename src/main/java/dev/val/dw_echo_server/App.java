package dev.val.dw_echo_server;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class App extends Application<ServiceConfiguration> {

    public static void main(String... args) throws Exception {
        new App().run(args);
    }

    public void run(ServiceConfiguration serviceConfiguration, Environment environment) throws Exception {
        EchoApplication echoApplication = new EchoApplication();
        environment.jersey().register(new EchoResource(echoApplication));
    }
}
