package dev.val.dw_echo_server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("v1")
public class EchoResource {
    private final EchoApplication echoApplication;

    EchoResource(EchoApplication echoApplication) {
        this.echoApplication = echoApplication;
    }

    @GET
    @Path("/echo")
    @Produces(MediaType.APPLICATION_JSON)
    public String echo() {
        return echoApplication.getEchoValue();
    }
}
